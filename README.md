# Sentiment_with_BERT

* [Sentiment_with_BERT Example Code](https://github.com/eniompw/Sentiment_with_BERT/blob/main/Sentiment_with_BERT.ipynb)
* [bert.zip Saved Model generated from this Tutorial](https://www.tensorflow.org/text/tutorials/classify_text_with_bert)
* [Flask App Sentiment Analysis](https://github.com/eniompw/Sentiment_with_BERT/blob/main/flask_app.py)
